# Prosody Moderno

El objetivo de este proyecto es tener un servidor de XMPP listo para
usar, con todos los XEPs necesarios para facilitar el uso en clientes
modernos como Conversations y Gajim.

## Uso

Modificar todas las líneas `ENV` del Dockerfile para adaptarlo a nuestra
configuración.

Luego, crear la imagen:

```bash
docker build -t prosody:1 .
```

Y correrla:

```bash
docker run -d prosody:1
```

## TODO

[Ver listado de pendientes](https://0xacab.org/partido-interdimensional-pirata/prosody-moderno/issues)
