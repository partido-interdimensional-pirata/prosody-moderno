# Usamos Alpine como imagen intermedia para aplicar las plantillas y
# descargar los módulos necesarios.
FROM alpine:3.7 AS build

# Definir estas variables antes de compilar el contenedor!
# La usuaria administradora
ENV ADMIN root
# El nombre de dominio
ENV HOSTNAME prosody.moderno
# El subdominio para archivos compartidos
ENV UPLOADS upload
# El subdominio para canales
ENV MUCS muc
# Requerir cifrado para las conexiones cliente-servidor
ENV C2S_ENCRYPTION true
# Requerir cifrado para las conexiones servidor-servidor
ENV S2S_ENCRYPTION true
# Verificar los certificados de los servidores (no permite MITM)
ENV S2S_SECURE_AUTH true
# Permitir registros adhoc
ENV ALLOW_REGISTRATION true
# Tiempo de almacenamiento de los mensajes archivados (es decir el
# tiempo que puede estar desconectada una persona sin perder mensajes)
ENV MAM_ARCHIVE 1w

# Instalar los requisitos
RUN apk add --no-cache gettext mercurial
# Trabajar dentro de este directorio
WORKDIR /root
# Clonar el repositorio de módulos en el directorio /root/modules
RUN hg clone https://hg.prosody.im/prosody-modules/ modules
# Copiar la plantilla de la configuración aquí
COPY prosody.cfg.lua.in .
# Reemplazar todas las variables de la plantilla por las variables de
# entorno
RUN envsubst < prosody.cfg.lua.in > prosody.cfg.lua

# Crear la imagen final a partir de esta, que ya viene configurada para
# correr monit desde el inicio.
FROM registry.forja.lainventoria.com.ar/habitapp/monit

# Los ENV se repiten porque después de cada FROM se vacían
ENV HOSTNAME prosody.moderno
ENV MODULES /usr/lib/prosody/modules/

# Paquetes a instalar dentro de la imagen final
RUN apk add --no-cache su-exec prosody ca-certificates libressl
# La configuración de monit se encargar de mantener prosody andando
COPY monit.conf /etc/monit.d/prosody.conf
# Copiar solo los módulos de Prosody que vamos a usar al directorio de
# módulos
COPY --from=build /root/prosody.cfg.lua /etc/prosody/prosody.cfg.lua
COPY --from=build /root/modules/mod_http_upload/mod_http_upload.lua ${MODULES}
COPY --from=build /root/modules/mod_mam_muc/mod_mam_muc.lua ${MODULES}
COPY --from=build /root/modules/mod_omemo_all_access/mod_omemo_all_access.lua ${MODULES}
COPY --from=build /root/modules/mod_delay/mod_delay.lua ${MODULES}
COPY --from=build /root/modules/mod_csi_battery_saver/mod_csi_battery_saver.lua ${MODULES}
COPY --from=build /root/modules/mod_http_avatar/mod_http_avatar.lua ${MODULES}
COPY --from=build /root/modules/mod_smacks/mod_smacks.lua ${MODULES}
COPY --from=build /root/modules/mod_csi/mod_csi.lua ${MODULES}

# TODO solo generamos el certificado así hasta que implementemos Let's
# Encrypt
USER prosody
RUN prosodyctl cert generate ${HOSTNAME}

# Por alguna razón todo lo demás se sigue corriendo como prosody y la
# generación del contenedor falla :/
USER root

# Mantener la configuración y datos de las usuarias entre reinicios del
# contenedor.
VOLUME ["/var/lib/prosody"]

# Puertos usados por Prosody
EXPOSE 5000 5280 5222 5269

# No tenemos ENTRYPOINT ni CMD porque usamos el del contenedor de monit.
